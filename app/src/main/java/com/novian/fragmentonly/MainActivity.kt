package com.novian.fragmentonly

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnFragment.setOnClickListener{
            val intent = Intent(this, Practice2ForFragmentActivity::class.java)
            startActivity(intent)
        }
    }
}
